import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

@Component({
  selector: 'myapp',
 /* template: `<div class="card card-block" *ngFor="let obj of objecto">
    <h2 class="card-title">{{obj.titulo}}</h2>
    <p class="card-text" >{{obj.mensaje}}</p>
  </div>`*/
  templateUrl:'/application/views/bucle/template_titulo.php'
})
class MyappComponent {
  objecto: Object[];
  
  constructor() {
    this.objecto = [
      {
        titulo: "Vencer el mal",
        mensaje: "El mal se vence alciendo el bien"
      },
      {
        titulo: "Secreto del amor",
        mensaje: "Sólo se puede amar a otros, cuando se tiene amor propio"
      },
      {
        titulo: "El perdón",
        mensaje: "Mira los niños, ellos olvidan las ofensas porque su mente solo retiene lo importante, que les permite crecer y aprender"
      }
    ];
  }
}

@NgModule({
  imports: [BrowserModule],
  declarations: [MyappComponent],
  bootstrap: [MyappComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);