import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

@Component({
  selector: 'myapp',
 /* template: `<div class="card card-block" *ngFor="let obj of objecto">
    <h2 class="card-title">{{obj.titulo}}</h2>
    <p class="card-text" >{{obj.mensaje}}</p>
  </div>`*/
  templateUrl:'/application/views/angular2/template.php'
})
class MyappComponent {
  objecto: Object[];
  
  constructor() {
    this.objecto = [
      {Nombre:'Corazón Valiente',Genero:'Histórica',Protagonista:'Mel Gibson'},
      {Nombre:'Iron Man',Genero:'Acción',Protagonista:'Robert Downey'}
    ];
  }
  orderByMe(x) {
        //$scope.myOrderBy = x;
    }
	seleccionFiltro (x) {
       // $scope.myFiltro = x;
    }
    removeItem (x) {
       // $scope.errortext = "";    
        this.objecto.splice(x, 1);
    }

  addItem () {
     /*  
        if (!$scope.add_nombre) {return;}
		
        if (findWithAttr($scope.products, 'Nombre', $scope.add_nombre) == -1) {
            var element = {};
			element.Nombre =$scope.add_nombre;
			element.Genero = $scope.add_genero;
			element.Protagonista=$scope.add_protag;
			$scope.products.push(element);
            
            
        } else {
            $scope.errortext = "Ya existe en la lista.";
        }*/
    var element = {};
      element.Nombre ="Nom prueba";
			element.Genero = "genero prueba";
			element.Protagonista="protag prueba";
			this.objecto.push(element);
  }
  
  }//EOC

@NgModule({
  imports: [BrowserModule],
  declarations: [MyappComponent],
  bootstrap: [MyappComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);platformBrowserDynamic