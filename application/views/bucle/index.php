<!DOCTYPE html>
<html>
<head>
  <title>Base Angular 2</title>
  <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css">

  <script src="https://unpkg.com/core-js/client/shim.min.js"></script>
  <script src="https://unpkg.com/zone.js@0.7.4?main=browser"></script>
  <script src="https://unpkg.com/reflect-metadata@0.1.8"></script>
  <script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>
  <script src="/assets/js/bucle/systemjs.config.js"></script>
  <script>
    System.import('/assets/js/bucle/script.ts').catch(function (err) {
      console.error(err);
    });
  </script>
</head>
<body>
  <h1>Angular 2 Interpolacion y Bucles</h1>
  <myapp></myapp>
</body>
</html>