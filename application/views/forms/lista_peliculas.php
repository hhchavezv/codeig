<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<head>
<script>
var app = angular.module("peliculas", []); 
app.controller("myCtrl", function($scope) {
    $scope.products = [
        {Nombre:'Corazón Valiente',Genero:'Histórica',Protagonista:'Mel Gibson'},
        {Nombre:'Iron Man',Genero:'Acción',Protagonista:'Robert Downey'}
      
        ];
    $scope.addItem = function () {
        $scope.errortext = "";
        if (!$scope.add_nombre) {return;}
		
        if (findWithAttr($scope.products, 'Nombre', $scope.add_nombre) == -1) {
            var element = {};
			element.Nombre =$scope.add_nombre;
			element.Genero = $scope.add_genero;
			element.Protagonista=$scope.add_protag;
			$scope.products.push(element);
            
            
        } else {
            $scope.errortext = "Ya existe en la lista.";
        }
    }
	$scope.orderByMe = function(x) {
        $scope.myOrderBy = x;
    }
	$scope.seleccionFiltro = function(x) {
        $scope.myFiltro = x;
    }
    $scope.removeItem = function (x) {
        $scope.errortext = "";    
        $scope.products.splice(x, 1);
    }
});

function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

</script>
</head>

<body>
<div ng-app="peliculas" ng-cloak ng-controller="myCtrl"  style="max-width:400px;">
  
    <h3>Lista Películas</h3>
 


 
<table border="border-collapse: collapse; border: 1px solid #000;" width="100%">
	<tr>
		<td>
		<input type="text" placeholder="Filtrar nombre" name="nombre" id="nombre" ng-model="filtro_nom" ng-change="seleccionFiltro('filtro_nom')"></input>
		</td>
		<td>
		<input type="text" placeholder="Filtrar genero"name="genero" id="genero" ng-model="filtro_genero"></input>
		</td>
		<td>
		<input type="text" placeholder="Filtrar protagonista" name="protag" id="protag" ng-model="filtro_protag"></input>
		</td>
	</tr>
	<tr>
		<th ng-click="orderByMe('Nombre')" style="text-decoration: underline;">Nombre</th>
		<th ng-click="orderByMe('Genero')" style="text-decoration: underline;">Género</th>
		<th ng-click="orderByMe('Protagonista')"style="text-decoration: underline;">Protagonista</th>
	</tr>
 <tr ng-repeat="x in products | orderBy:myOrderBy |  filter : filtro_nom |  filter : filtro_genero |  filter : filtro_protag">
<!-- <tr ng-repeat="x in products | orderBy:myOrderBy |  filter :myFiltro ">-->
<td>{{x.Nombre}}</td>
<td>{{x.Genero}}</td>
<td>{{x.Protagonista}}</td>
<td><span ng-click="removeItem($index)" style="cursor:pointer;">Eliminar</span></td>
</tr>
</table>
<br>
 <h4> Añadir Película </h4> 
 <table>
	<tr> <td><input placeholder="Nombre de pelicula" ng-model="add_nombre"  ></td></tr>
	<tr><td><input placeholder="Género de pelicula" ng-model="add_genero"  ></td></tr>
	<tr><td><input placeholder="Protagonista de pelicula" ng-model="add_protag"  ></td></tr>
	<tr> <td>  <button ng-click="addItem()" class="w3-btn w3-padding w3-green">Add</button></td></tr>
  </table>
   
    <p class="w3-text-red">{{errortext}}</p>
 
</body>
</html>
