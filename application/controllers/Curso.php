<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends CI_Controller {

   function __construct()
	{
		parent::__construct();
	}
  
	public function index()
	{
    $data["view"] = "dashboard";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('layout', $data);
	}
  
  public function formulario()
	{
    $data["view"] = "formulario";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('layout', $data);
	}
  
  public function control()
	{
    $data["view"] = "control";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('layout', $data);
	}
	
	public function basicos($params = 'index')
	{
    $data["view"] = $params;
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/basicos/' . $params, $data);
	}
	
	public function calculadora($params = 'index')
	{
    $data["view"] = $params;
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/calculadora/' . $params, $data);
	}
	public function forms($params = 'lista_peliculas')
	{
    $data["view"] = $params;
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/forms/' . $params, $data);
	}
	public function bucle($params = 'index')
	{
    $data["view"] = $params;
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/bucle/' . $params, $data);
	}
		public function angular2($params = 'index')
	{
    $data["view"] = $params;
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/angular2/' . $params, $data);
	}
	
	
	public function angular($params = 'index')
{
	$data["view"] = "angular";
	$this->load->view('angular', $data);
}

public function testangular($params = 'index')
{
	$data["view"] = $params;
	$this->load->view('/testangular/' . $params, $data);
}
	
}
